// noinspection JSAnnotator
import db from "../firebase/firebase";

let storedQuestions = JSON.parse(localStorage.getItem('questions')) || [];


export default (state = storedQuestions, action) => {
    switch (action.type) {
        case 'SELECTED_ANSWER':
            return state;
        case 'LOAD_QUESTIONS':
            localStorage.setItem("questions" , JSON.stringify(state));
            return [
                ...state,
                action.allQuestions
            ];
        default:
            return state;
    }
};


// const defaultState = [
//     {
//         text: "How much time does the student spend waiting for the bus ? ",
//         id: 0,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes",
//             2: "10 minutes",
//             3: "3 minutes",
//             4: "15 minutes",
//             5: "8 minutes"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 1? ",
//         id: 1,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_1",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_1",
//             2: "10 minutes_1",
//             3: "3 minutes_1",
//             4: "15 minutes_1",
//             5: "8 minutes_1"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 2? ",
//         id: 2,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_2",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_2",
//             2: "10 minutes_2",
//             3: "3 minutes_2",
//             4: "15 minutes_2",
//             5: "8 minutes_2"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 3? ",
//         id: 3,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_3",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_3",
//             2: "10 minutes_3",
//             3: "3 minutes_3",
//             4: "15 minutes_3",
//             5: "8 minutes_3"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 4? ",
//         id: 4,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_4",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_4",
//             2: "10 minutes_4",
//             3: "3 minutes_4",
//             4: "15 minutes_4",
//             5: "8 minutes_4"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus ? ",
//         id: 5,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes",
//             2: "10 minutes",
//             3: "3 minutes",
//             4: "15 minutes",
//             5: "8 minutes"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 1? ",
//         id: 6,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_1",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_1",
//             2: "10 minutes_1",
//             3: "3 minutes_1",
//             4: "15 minutes_1",
//             5: "8 minutes_1"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 2? ",
//         id: 7,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_2",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_2",
//             2: "10 minutes_2",
//             3: "3 minutes_2",
//             4: "15 minutes_2",
//             5: "8 minutes_2"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 3? ",
//         id: 8,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_3",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_3",
//             2: "10 minutes_3",
//             3: "3 minutes_3",
//             4: "15 minutes_3",
//             5: "8 minutes_3"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 4? ",
//         id: 9,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_4",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_4",
//             2: "10 minutes_4",
//             3: "3 minutes_4",
//             4: "15 minutes_4",
//             5: "8 minutes_4"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus ? ",
//         id: 10,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes",
//             2: "10 minutes",
//             3: "3 minutes",
//             4: "15 minutes",
//             5: "8 minutes"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 1? ",
//         id: 11,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_1",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_1",
//             2: "10 minutes_1",
//             3: "3 minutes_1",
//             4: "15 minutes_1",
//             5: "8 minutes_1"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 2? ",
//         id: 12,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_2",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_2",
//             2: "10 minutes_2",
//             3: "3 minutes_2",
//             4: "15 minutes_2",
//             5: "8 minutes_2"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 3? ",
//         id: 13,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_3",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_3",
//             2: "10 minutes_3",
//             3: "3 minutes_3",
//             4: "15 minutes_3",
//             5: "8 minutes_3"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 4? ",
//         id: 14,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_4",
//         points: 4,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_4",
//             2: "10 minutes_4",
//             3: "3 minutes_4",
//             4: "15 minutes_4",
//             5: "8 minutes_4"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus ? ",
//         id: 15,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes",
//         points: 8,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes",
//             2: "10 minutes",
//             3: "3 minutes",
//             4: "15 minutes",
//             5: "8 minutes"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 1? ",
//         id: 16,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_1",
//         points: 8,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_1",
//             2: "10 minutes_1",
//             3: "3 minutes_1",
//             4: "15 minutes_1",
//             5: "8 minutes_1"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 2? ",
//         id: 17,
//         graph: "travel_chart_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_2",
//         points: 8,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_2",
//             2: "10 minutes_2",
//             3: "3 minutes_2",
//             4: "15 minutes_2",
//             5: "8 minutes_2"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 3? ",
//         id: 18,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_3",
//         points: 8,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_3",
//             2: "10 minutes_3",
//             3: "3 minutes_3",
//             4: "15 minutes_3",
//             5: "8 minutes_3"
//         }
//     },
//     {
//         text: "How much time does the student spend waiting for the bus 4? ",
//         id: 19,
//         graph: "cell_grid_1",
//         isCorrect: false,
//         correctAnswer: "5 minutes_4",
//         points: 8,
//         selectedAnswer: "",
//         answerOptions: {
//             1: "5 minutes_4",
//             2: "10 minutes_4",
//             3: "3 minutes_4",
//             4: "15 minutes_4",
//             5: "8 minutes_4"
//         }
//     }
// ]
// noinspection JSAnnotator

export default (state = {totalPoints: 0}, action) => {
    switch (action.type) {
        case 'TOTAL_POINTS':
        return {
            ...state,
            totalPoints: state.totalPoints + action.total
        }
        default:
            return state;
    }
};
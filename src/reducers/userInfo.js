// noinspection JSAnnotator

let storedOrder = JSON.parse(localStorage.getItem("order")) || 0;
let parsedMin = JSON.parse(localStorage.getItem("min")) < 6 ? JSON.parse(localStorage.getItem("min")) : 5;
let storedMin =  parsedMin === null ? 20 : parsedMin;
let storedSec = JSON.parse(localStorage.getItem("sec")) || 0;
let storedUserInfo = JSON.parse(localStorage.getItem("userInfo")) || {};
let storedCompleted = JSON.parse(localStorage.getItem("completed")) || false ;
let startedStored = JSON.parse(localStorage.getItem("started")) || false ;
let storedCurp = JSON.parse(localStorage.getItem("curp")) || false;
const defaultState = {
    order: storedOrder,
    min: storedMin,
    sec: storedSec,
    userId: 12345,
    userInfo: storedUserInfo,
    completed: storedCompleted,
    started: startedStored,
    curp: storedCurp,
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'NEXT_QUESTION':
            let orderNumber = state.order + 1
            localStorage.setItem("order" , JSON.stringify(orderNumber))
            return {
                ...state,
                order: orderNumber
            };
        case 'COUNTDOWN_MIN':
            return state

        case 'COUNTDOWN_SEC':
            let secDisplayed;
            if (state.sec < 1 && state.min !== 0) {
                secDisplayed = 59;
            } else if (state.sec === 0 && state.min === 0 ) {
                secDisplayed = 0;
            } else {
                secDisplayed = state.sec - 1
            }
            let min;
            if (state.sec < 1 && state.min !== 0) {
                min = state.min - 1;
            } else if (state.sec === 0 && state.min === 0 ) {
                min = 0;
            } else {
                min = state.min
            }
            localStorage.setItem("min" , JSON.stringify(min));
            localStorage.setItem("sec" , JSON.stringify(secDisplayed));
            return {
                ...state,
                sec: secDisplayed,
                min: min
            };
        case 'ADD_USER_INFO':
            localStorage.setItem("userInfo" , JSON.stringify(action.userInfo));
            return {
                ...state,
                userInfo: action.userInfo
            };
        case 'ADD_CURP':
            localStorage.setItem("curp" , JSON.stringify(action.curp));
            console.log(action.curp);
            return {
                ...state,
                curp: action.curp,
            };
        case 'ADD_COMPLETED':
            localStorage.setItem("completed" , JSON.stringify(true));
            return {
                ...state,
                completed: true,
            };
        case 'CLEAR_STORAGE':
            localStorage.setItem('started', JSON.stringify(true));
            localStorage.removeItem('answers');
            localStorage.removeItem('questions');
            localStorage.removeItem('min');
            localStorage.removeItem('sec');
            localStorage.removeItem('userInfo');
            localStorage.removeItem('order');
            return {
                ...state,
                sec: 0,
                min: 20,
                order: 0,
                started: true
            };
        case 'CLEAR_START':
            localStorage.removeItem('answers');
            localStorage.removeItem('questions');
            localStorage.removeItem('min');
            localStorage.removeItem('sec');
            localStorage.removeItem('userInfo');
            localStorage.removeItem('order');
            return {
                ...state,
                sec: 0,
                min: 20,
                order: 0,
            };
        default:
            return state;
    }
};
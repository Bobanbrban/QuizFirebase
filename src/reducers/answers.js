// noinspection JSAnnotator

let storedAnswers = JSON.parse(localStorage.getItem('answers'));

const defaultState = storedAnswers || [];

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ANSWERS_UPDATE':
            return [
                ...state,
              action.answer
            ]
        case 'LOAD_ANSWERS':
            console.log(action)
            return [
                ...state=[],
                ...action.answers
                ]
        case 'LOAD_ANSWERS_DONE':
            localStorage.removeItem('answers');
            return [
                ...state=[],
                ...action.answers
            ]
        default:
            return state;
    }
};
import  firebase  from 'firebase';
import 'firebase/firestore';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyD2IMI9i9Gw6mskN9PlCNbmk7vDMzGpNYA",
    authDomain: "quizapp-d71a8.firebaseapp.com",
    databaseURL: "https://quizapp-d71a8.firebaseio.com",
    projectId: "quizapp-d71a8",
    storageBucket: "quizapp-d71a8.appspot.com",
    messagingSenderId: "480458582841"
};

firebase.initializeApp(config);
const db = firebase.firestore();

let provider = new firebase.auth.GoogleAuthProvider();
provider.addScope('profile');
provider.addScope('email');

const settings = {/* your settings... */ timestampsInSnapshots: true};
db.settings(settings);

export { firebase, provider };
export default db;


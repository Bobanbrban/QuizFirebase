import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import {connect} from "react-redux";
import createHistory from 'history/createBrowserHistory';
import QuestionBody from './pages/question-body';
import Admin from "./pages/admin";

export const history = createHistory();

const QuizRouter = () => (
    <Router history={history}>
        <div>
            <Switch>
                <Route path="/" component={ QuestionBody } exact={true}/>
                <Route path="/admin" component={Admin}/>
            </Switch>
        </div>
    </Router>
);


const mapStatetoProps = (state) => ({
   completed: state.order.completed
})


export default connect(mapStatetoProps)(QuizRouter);

import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import questions from '../reducers/questions';
import userInfo from '../reducers/userInfo';
import answers from '../reducers/answers';
import totalPoints from '../reducers/totalPoints';
import questionId from '../reducers/adminReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            questions: questions,
            order: userInfo,
            answers: answers,
            total: totalPoints,
            questionId: questionId
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
};

import React from 'react';
import { connect } from 'react-redux';
import ResultsElements from "./ResultsElements";
import { totalPoints } from "../actions/actions";



let Results = (props) => {

        let answers = props.answers ;
        let resultsList;
        resultsList = answers.map((answer, i) => {
            return (
                 <ResultsElements
                     answerLi={answer}
                     key={i}
                     result={answer.answer === answer.correctAnswer ? props.totalPoints({total: answer.points})
                     && answer.points : 0}
                 />
             )
         })
        return (
            <ul className="resultsList">
                {resultsList}
            </ul>
        )
}

const mapStatetoProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
    totalPoints: (total) => dispatch(totalPoints(total)),
});


export default connect( mapStatetoProps, mapDispatchToProps )(Results);

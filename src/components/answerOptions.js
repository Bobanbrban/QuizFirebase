import React from 'react';


class AnswerOptions extends React.Component {

    toggle = (id) => {
        let checkedBox = document.querySelectorAll("#checkBox");
        let options = document.querySelectorAll("#option");
        checkedBox.forEach(box => box.dataset.index == id ? box.checked = true : box.checked = false);
        options.forEach((option, i) => i == id ? option.classList.add("checked") : option.classList.remove("checked"));
    }


    render() {
        let background =  this.props.options.background;
        let backgroundExist = background !== undefined ? true : false;
        let image1 =  backgroundExist ? require ( `../content/${background[1]}.png`) : '';
        let image2 =  backgroundExist ? require ( `../content/${background[2]}.png`) : '';
        let image3 =  backgroundExist ? require ( `../content/${background[3]}.png`) : '';
        let image4 =  backgroundExist ? require ( `../content/${background[4]}.png`) : '';
        let image5 =  backgroundExist ? require ( `../content/${background[5]}.png`) : '';

        let style1 = {backgroundImage: 'url(' + image1 + ')', backgroundSize: 'cover', minHeight : "60px" , minWidth : "60px", height: "10vw" , width: "10vw"};
        let style2 = {backgroundImage: 'url(' + image2 + ')', backgroundSize: 'cover', minHeight : "60px" , minWidth : "60px", height: "10vw" , width: "10vw"};
        let style3 = {backgroundImage: 'url(' + image3 + ')', backgroundSize: 'cover', minHeight : "60px" , minWidth : "60px" , height: "10vw" , width: "10vw"};
        let style4 = {backgroundImage: 'url(' + image4 + ')', backgroundSize: 'cover', minHeight : "60px" , minWidth : "60px", height: "10vw" , width: "10vw"};
        let style5 = {backgroundImage: 'url(' + image5 + ')', backgroundSize: 'cover', minHeight : "60px" , minWidth : "60px", height: "10vw" , width: "10vw"};
        return (
            <div className="answerOptions">
                <div className="option1" id="option" onClick={() => this.toggle(0)} style={backgroundExist ? style1 : {}}>
                    <div className="answerText" >
                        {this.props.options[1]}
                    </div>
                    <div className="squaredThree squaredThree1">
                        <input type="checkbox" value="None" id="checkBox" name="check" data-index="0"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option2" id="option" onClick={() => this.toggle(1)} style={backgroundExist ? style2 : {}}>
                    <div className="answerText">
                        {this.props.options[2]}
                    </div>
                    <div className="squaredThree squaredThree2">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="1"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option3" id="option" onClick={() => this.toggle(2)} style={backgroundExist ? style3 : {}}>
                    <div className="answerText">
                        {this.props.options[3]}
                    </div>
                    <div className="squaredThree squaredThree3">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="2"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option4" id="option" onClick={() => this.toggle(3)} style={backgroundExist ? style4 : {}}>
                    <div className="answerText">
                        {this.props.options[4]}
                    </div>
                    <div className="squaredThree squaredThree4">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="3"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
                <div className="option5" id="option" onClick={() => this.toggle(4)} style={backgroundExist ? style5 : {}}>
                    <div className="answerText">
                        {this.props.options[5]}
                    </div>
                    <div className="squaredThree squaredThree5">
                        <input type="checkbox" value="None"  id="checkBox" name="check" data-index="4"/>
                        <label htmlFor="squaredThree"/>
                    </div>
                </div>
            </div>
        )
    }
}



export default AnswerOptions;
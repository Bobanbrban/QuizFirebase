import React from 'react';
import { connect } from 'react-redux';
import {newQuestion, questionId} from '../../actions/adminActions'



class Admin extends React.Component  {
      constructor(props) {
          super(props);
          this.state = {
             graph: "graphs/travel_chart_1",
          }
      }

      componentDidMount() {
          let questionGroup = document.querySelector(".question-group").value;
          this.props.questionId({questionGroup});
      }

      questionIdChange = () => {
          let questionGroup = document.querySelector(".question-group").value;
          this.props.questionId({questionGroup});
      }

    pushNewQuestion = () => {
        let questionGroup = document.querySelector(".question-group").value || '';
        let questionText = document.querySelector(".question-text").value || '';
        let answerOption1 = document.querySelector(".answer-option-1").value || '';
        let answerOption2 = document.querySelector(".answer-option-2").value || '';
        let answerOption3 = document.querySelector(".answer-option-3").value || '';
        let answerOption4 = document.querySelector(".answer-option-4").value || '';
        let answerOption5 = document.querySelector(".answer-option-5").value || '';

        let background1 = document.querySelector(".background-option-1").value || '';
        let background2 = document.querySelector(".background-option-2").value || '';
        let background3 = document.querySelector(".background-option-3").value || '';
        let background4 = document.querySelector(".background-option-4").value || '';
        let background5 = document.querySelector(".background-option-5").value || '';

        let backgroundOptions = {1: background1 , 2: background2, 3: background3 , 4: background4, 5: background5};
        let option = {1: answerOption1 , 2: answerOption2, 3: answerOption3 , 4: answerOption4, 5: answerOption5, background: backgroundOptions };

        let correctAnswer = document.querySelector(".correctAnswer").value || '';
        let pointsValue = document.querySelector(".points-value").value || '';
        let questionId = this.props.id.id || '';
        let graph = document.querySelector(".graph").value || '';
        let questionObject = {text : questionText, answerOptions : option, correctAnswer, graph, questionId, points : pointsValue, selectedAnswer: '', questionGroup} || {};
        setTimeout(() => this.props.newQuestion(questionObject), 0);
        console.log(questionObject);
    }

     loadGraph = () => {
         let graphImage = document.querySelector(".graph").value || '';
         this.setState({
             graph: graphImage
         })
     }


    render() {
        let graphValue = this.state.graph;
        let image = require ( `../../content/${graphValue}.png` || '');
        return (
            <div className="adminPage-body">
                <form className="questionsInputs">
                    <div className="questionInfo">
                        <div className="questionId"> Question Id {this.props.id.id} </div><br/>
                        <input type="text" className="correctAnswer" placeholder="correctAnswer"/> <br/>
                        <select type="text" className="question-group" placeholder="question-group" onChange={this.questionIdChange}>
                            <option className="selectedGroup" value="logical">logical</option>
                            <option className="selectedGroup" value="numerical">numerical</option>
                            <option className="selectedGroup" value="complex">complex</option>
                            <option className="selectedGroup" value="pattern">pattern</option>
                            <option className="selectedGroup" value="special">special</option>
                        </select> <br/>
                        <select type="text" className="points-value" placeholder="points-value">
                            <option value="4">4 points</option>
                            <option value="8">8 points</option>
                        </select>
                    </div>
                    <div className="imageDiv">
                        <img className="inputGraph" src={image} alt="graph"/>
                        <input type="text" className="graph" placeholder="graph"/>
                        <button type="button" className="loadGraph" onClick={this.loadGraph}>Load graph</button>
                    </div>
                    <div className="questionDiv">
                        <input type="text" className="question-text" placeholder="question text"/> <br/>
                        <input type="text" className="answer-option-1" name="options" placeholder="answer-option-1" data-index="1"/>
                        <input type="text" className="answer-option-2" name="options" placeholder="answer-option-2" data-index="2"/>
                        <input type="text" className="answer-option-3" name="options" placeholder="answer-option-3" data-index="3"/>
                        <input type="text" className="answer-option-4" name="options" placeholder="answer-option-4" data-index="4"/>
                        <input type="text" className="answer-option-5" name="options" placeholder="answer-option-5" data-index="5"/> <br/> <br/>
                        <input type="text" className="background-option-1" name="background" placeholder="backgroundImg-option-1" data-index="1"/>
                        <input type="text" className="background-option-2" name="background" placeholder="backgroundImg-option-2" data-index="2"/>
                        <input type="text" className="background-option-3" name="background" placeholder="backgroundImg-option-3" data-index="3"/>
                        <input type="text" className="background-option-4" name="background" placeholder="backgroundImg-option-4" data-index="4"/>
                        <input type="text" className="background-option-5" name="background" placeholder="backgroundImg-option-5" data-index="5"/> <br/>
                        <button type="button" className="submitAdmin" onClick={this.pushNewQuestion}>Add Question</button>
                    </div>
                </form>
                <div className="questionsList">
                </div>
            </div>
        )
    }
}

const mapStatetoProps = (state) => ({
     id: state.questionId
});

const mapDispatchToProps = (dispatch) => ({
    newQuestion: (questionObject) => dispatch(newQuestion(questionObject)),
    questionId: (group) => dispatch(questionId(group)),
});

export default connect(mapStatetoProps, mapDispatchToProps)(Admin);
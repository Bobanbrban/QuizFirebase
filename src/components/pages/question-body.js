import React from 'react';
import AnswerOptions from "../answerOptions";
import ResultsPage from "./resultsPage"
import db from '../../firebase/firebase'
import CircularProgressbarWithGradient from "../progerssbar";
import { connect } from 'react-redux';
import modal from "modals";
import {
    nextQuestion,
    countDownSec,
    selectedAnswerText,
    allAnswers,
    startLoadQuestions,
    clearLocalStorage,
    addUserInfo,
    loadAnswers,
    startAllAnswers,
    addCurp,
    addCompleted,
    clearOnStart
} from "../../actions/actions"
import {startLogin} from "../../actions/auth";


class QuestionBody extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            selected: 0,
        }
    }
        componentWillMount() {
           let getCloudfuncuion = () => {
               fetch('https://us-central1-quizapp-d71a8.cloudfunctions.net/helloWorld')
                   .then(blob => blob.json())
                   .then(data => console.log(data))
           };
           getCloudfuncuion();

            let pushToHistory = () => {
                if(!this.props.started && !this.props.completed) {
                    this.props.clearOnStart();
                } else if (this.props.started && !this.props.completed) {
                    let interval = setInterval(() => this.props.countDownSec(), 1000);
                    setTimeout(() => clearInterval(interval), 305000);
                }
            };
            pushToHistory();
        }

    componentDidMount() {
          let changeSelection = () => {
              let checked = document.querySelector('.infoText');
              if (!this.props.started && !this.props.completed) {
                  if (this.state.selected === 0) {
                      checked.children[9].classList.remove('hlLogin');
                      checked.children[this.state.selected].classList.add('hlLogin');
                      this.setState({
                          selected: this.state.selected + 1,
                      });
                  } else if (this.state.selected > 0 && this.state.selected < 10) {
                      checked.children[this.state.selected - 1].classList.remove('hlLogin');
                      checked.children[this.state.selected].classList.add('hlLogin');
                      this.setState({
                          selected: this.state.selected + 1,
                      });
                  }
              }
          };
          changeSelection();
          setInterval(() => changeSelection(), 4000);
    }

    onSubmit = () => {
        this.props.startLogin()
            .then(ref => {
                let userInfo = ref.additionalUserInfo.profile;
                let curp = this.props.curp;
                let userRef = db.doc(`users/${curp}`);
                userRef.get()
                    .then(doc => {
                        if (doc.exists && (new Date().getTime() - doc.data().valueOf().timestamp > 5184000000)) {
                            this.props.addUserInfo(userInfo);
                            this.props.clearLocalStorage();
                            this.props.startLoadQuestions(userInfo);
                            let interval = setInterval(() => this.props.countDownSec(), 1000);
                            setTimeout(() => clearInterval(interval), 305000);
                        } else if (doc.exists && (new Date().getTime() - doc.data().valueOf().timestamp < 5184000000)) {
                            this.props.addCompleted();
                        } else {
                            this.props.addUserInfo(userInfo);
                            this.props.clearLocalStorage();
                            this.props.startLoadQuestions(userInfo);
                            let interval = setInterval(() => this.props.countDownSec(), 1000);
                            setTimeout(() => clearInterval(interval), 305000);
                        }
                    })
                    .catch(err => console.log(err));
            })
    };


    addAnswer = () => {
        let checkedBox = document.querySelectorAll("#checkBox");
        let qustionNumber = this.props.number.order;
        let ckecked = document.querySelectorAll('#checkBox');
        let checkedArray = [...ckecked];
        let indexes = checkedArray.findIndex(e => e.checked );
        let answerOptions = this.props.state[qustionNumber].question.answerOptions;
        let selectedAnswer = '';
        if(answerOptions.background) {
            selectedAnswer = this.props.state[qustionNumber].question.answerOptions.background[indexes + 1];
        } else {
            selectedAnswer =  this.props.state[qustionNumber].question.answerOptions[indexes + 1];
        }
        console.log(selectedAnswer);
        let answer = {
            question: this.props.state[qustionNumber].question.text,
            correctAnswer: this.props.state[qustionNumber].question.correctAnswer,
            answer: selectedAnswer,
            questionId: this.props.state[qustionNumber].question.id,
            points: parseInt(this.props.state[qustionNumber].question.points),
            qustionOrderNumber: this.props.number.order + 1,
        };

        this.props.selectedAnswerText({
            answer: answer,
        });
        this.props.allAnswers({
            answer: answer,
        });
        checkedBox.forEach(box => box.checked = false);
        let options = document.querySelectorAll("#option");
        options.forEach(option => option.classList.remove("checked"));
        localStorage.setItem("answers", JSON.stringify(this.props.storedAnswers));
    };

    nextQuestion = () => {
        if(!this.props.started && !this.props.completed) {
            let curp = document.querySelector(".curpInput").value;
            let re = /^[a-zA-Z0-9]{4}$/;
            let OK = curp.match(re);
            if(OK) {
                this.props.addCurp(curp);
                this.onSubmit();
            } else {
                modal.alert({
                    message: 'CURP number is invalid',
                    callback: function () {
                        console.log('curp number is invalid')
                    },
                    okTitle: 'OK'
                })
            }
        }  else if (this.props.started && !this.props.completed && this.props.number.order < 19 && ((this.props.number.min + this.props.number.sec)!== 0)) {
            this.addAnswer();
            this.props.nextQuestion();
        } else if ( this.props.number.order === 19 || ((this.props.number.min + this.props.number.sec) === 0)) {
            this.addAnswer();
            setTimeout(() => this.props.addCompleted(),0);
        }
    };

    next = () => {
        if(!this.props.started && !this.props.completed){
            return  "START"
        } else if (this.props.number.order ===  19 || ((this.props.number.min + this.props.number.sec) === 0)) {
            return  "SUBMIT"
        } else {
            return "NEXT"
        }
    };

    render() {
        let min = this.props.number.min;
        let sec = this.props.number.sec < 10 ? "0" + this.props.number.sec : this.props.number.sec;
        let qustionNumber = this.props.number.order;
         let image =  this.props.state[qustionNumber] !== undefined ? require ( `../../content/${this.props.state[qustionNumber].question.graph}.png`) : '';
        let percentage = (((qustionNumber + 1)/ 20) * 100);
        return <div className="body-container">
            { this.props.completed && <ResultsPage/> }
            { (!this.props.started && !this.props.completed) &&
            <div className="infoText">
                <p>
                  Welcome to the Agave Lab software development programing aptitude Quiz
                </p>
                <p>
                   Quiz has 20 questions and time limit is 20 minutes to answer them
                </p>
                <p>
                    Each correct answer can bring you 4 or 8 points with maximum total of 100 points
                </p>
                <p>
                    Each candidate can have one attempt within 2 months
                </p>
                <p>
                   If you have done quiz already, after pressing the start button, you will be redirected to the results page
                </p>
                <p>
                   Who scores 60 or more points, qualifies for second stage with 3 additional questions
                </p>
                <p>
                    If you qualify for second stage, after 10 seconds on result page you will be redirected to additional questions
                </p>
                <p>
                    Make sure that you have active google account witch you will be using during entire process
                </p>
                <p>
                    To begin with the quiz, please type your valid CURP number and press START button. Good Luck
                </p>
                <div className="curp">
                    <input className="curpInput" placeholder="CURP number" required/>
                </div>

            </div>
            }
            { (this.props.started && !this.props.completed) &&
                    <div>
                        <div className="questionTitle">
                            <div className="graphQuestion">
                                <img className="graphImage" src={image} alt="Loading image ..."/>
                            </div>
                            <div className="fullQuestion">
                                {this.props.state[qustionNumber] !== undefined ? this.props.state[qustionNumber].question.text : ''}
                            </div>
                        </div>
                       <AnswerOptions options={this.props.state[qustionNumber] !== undefined ? this.props.state[qustionNumber].question.answerOptions : []} image={image}/>
                    </div>
            }
            {!this.props.completed &&
            <div className="timeOrderNext">
                <div className="remainingTime">{min}:{sec}</div>
                <div className="questionNumber">
                    <CircularProgressbarWithGradient percentage={percentage} qustionNumber={qustionNumber}/>
                </div>
                <button className="next_question_button" onClick={this.nextQuestion}>{this.next()}</button>
            </div>
            }
        </div>
    }
}

const mapStatetoProps = (state) => ({
    state: state.questions,
    number: state.order,
    storedAnswers: state.answers,
    email: state.order.userInfo,
    started: state.order.started,
    completed: state.order.completed,
    userInfo: state.order.userInfo,
    answers: state.answers,
    total: state.total,
    curp: state.order.curp,
});

const mapDispatchToProps = (dispatch) => ({
    nextQuestion: () => dispatch(nextQuestion()),
    countDownSec: () => dispatch(countDownSec()),
    selectedAnswerText: (answer) => dispatch(selectedAnswerText(answer)),
    allAnswers: (answer) => dispatch(allAnswers(answer)),
    startLoadQuestions: () => dispatch(startLoadQuestions()),
    clearLocalStorage: () => dispatch(clearLocalStorage()),
    startLogin: () => dispatch(startLogin()),
    addUserInfo: (data) => dispatch(addUserInfo(data)),
    loadAnswers: (answers) => dispatch(loadAnswers(answers)),
    startAllAnswers: (allAnswersInfo) => dispatch(startAllAnswers(allAnswersInfo)),
    addCurp: (curp) => dispatch(addCurp(curp)),
    addCompleted: () => dispatch(addCompleted()),
    clearOnStart: () => dispatch(clearOnStart()),
});

export default connect(mapStatetoProps, mapDispatchToProps)(QuestionBody);

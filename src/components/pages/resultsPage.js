import React from 'react';
import { connect } from 'react-redux';
import Results from "../Results";
import db from "../../firebase/firebase"
import { startAllAnswers, startLoadAnswersDone, clearLocalStorage } from "../../actions/actions"


class ResultsPage extends React.Component {

    componentWillMount() {
        let compileResult = () => {
                if (this.props.curp) {
                    const userRef = db.doc(`users/${this.props.curp}`);
                    userRef.get()
                        .then(doc => {
                            if (doc.exists) {
                                userRef.get().then(data => {
                                    if (new Date().getTime() - doc.data().valueOf().timestamp > 5184000000) {
                                        let answers = this.props.answers;
                                        let totalObject = {total: this.props.total};
                                        let user = this.props.user;
                                        let curp = this.props.curp;
                                        const allAnswersInfo = {answers, totalObject, user, curp};
                                        this.props.startAllAnswers(allAnswersInfo);
                                    } else {
                                        let answers = data.data().valueOf().allAnswers;
                                        this.props.startLoadAnswersDone(answers);
                                    }
                                });
                            } else if (!doc.exists) {
                                let answers = this.props.answers;
                                let totalObject = {total: this.props.total};
                                let user = this.props.user;
                                let curp = this.props.curp;
                                const allAnswersInfo = {answers, totalObject, user, curp};
                                this.props.startAllAnswers(allAnswersInfo);
                            }
                        });
                }
        };
        compileResult();
    };

    componentDidMount() {
       this.props.clearLocalStorage();
    }


    render() {
        return <div className="resultsPage">
            <Results answers={this.props.answers}/>
            <div id="resultTotal">Total<br/>{this.props.total}/100</div>
        </div>
    }
}

const mapStatetoProps = (state) => ({
    answers: state.answers,
    total: state.total.totalPoints,
    user: state.order.userInfo,
    curp: state.order.curp
});

const mapDispatchToProps = (dispatch) => ({
    startAllAnswers: (allAnswersInfo) => dispatch(startAllAnswers(allAnswersInfo)),
    startLoadAnswersDone: (answers) => dispatch(startLoadAnswersDone(answers)),
    clearLocalStorage: () => dispatch(clearLocalStorage()),
});

export default connect(mapStatetoProps, mapDispatchToProps)(ResultsPage);
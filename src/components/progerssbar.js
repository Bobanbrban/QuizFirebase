import React from 'react';
import CircularProgressbar from 'react-circular-progressbar';
import PI from "mathjs";


function CircularProgressbarWithGradient(props) {

    let { percentage, qustionNumber } = props;
    let angle = percentage * 3.6;
    let rotation = {angle} * (PI.PI / 180);
    let startColor = "#F29F4E";
    let endColor = "#F29F4E";
    const gradientTransform = `rotate(${rotation || 0})`;

    return (
        <svg className="circular-progressbar" viewBox="0 0 100 100">
            <defs>
                <linearGradient id="gradient" gradientTransform={gradientTransform}>
                    <stop offset="0%" stopColor={startColor} />
                    <stop offset="100%" stopColor={endColor} />
                </linearGradient>
            </defs>
            <CircularProgressbar percentage={Math.floor(percentage)} strokeWidth={5} />
            <text className="label" x="50" y="50">
                {qustionNumber + 1} / 20
            </text>
        </svg>
    );
}

export default CircularProgressbarWithGradient;
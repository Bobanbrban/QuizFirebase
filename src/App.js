import React from 'react';
import { firebase } from  "./firebase/firebase";
import Quiz from './components/Quiz';
import { Provider } from 'react-redux';
import configureStore from './components/store';
import './stylesheets/style.css';

const store = configureStore();

const App = () =>  (
    <Provider store={store}>
        <Quiz />
    </Provider>
);

firebase.auth().onAuthStateChanged((user) => {
    if(user) {
        console.log('log in')
    } else {
        console.log('log out')
    }
})

export default App;


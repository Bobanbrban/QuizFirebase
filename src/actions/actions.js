import db from '../firebase/firebase';
import moment from 'moment';

export const nextQuestion = () => ({
    type: 'NEXT_QUESTION',
});


export const countDownSec = () => ({
    type: 'COUNTDOWN_SEC',
});

export const selectedAnswerText = ({answer}) => ({
    type: 'SELECTED_ANSWER',
    answer
});

export const allAnswers = ({answer}) => ({
    type: 'ANSWERS_UPDATE',
    answer
});

export const loadAnswers  = (answers) => ({
    type: 'LOAD_ANSWERS',
    answers
});

export const loadAnswersDone  = (answers) => ({
    type: 'LOAD_ANSWERS_DONE',
    answers
});

export const startLoadAnswersDone  = (answers) => {
    return (dispatch) => {
        return dispatch(loadAnswersDone(answers));
    }
};

export const startAllAnswers = (allAnswersInfo) => {
    return () => {
        const {answers, totalObject, user} = allAnswersInfo;
        let answersParsed = JSON.parse(JSON.stringify(answers));
            const userRef = db.doc(`users/${allAnswersInfo.curp}`);
                        userRef.set({
                            allAnswers: answersParsed,
                            user: user,
                            total: totalObject.total,
                            completed: true,
                            createdAt: moment().format('YYYY-MM-DD'),
                            timestamp: new Date().getTime(),
                            curp: allAnswersInfo.curp
                        })
                            .then(() => console.log("data was set to the database"))
                            .catch(error => console.error("Error adding document: ", error));
    }
};

// if (doc.exists) {
//     userRef.update({
//         allAnswers: answers,
//         user: user,
//         total: totalObject.total,
//         completed: true,
//         createdAt: moment().format('YYYY-MM-DD'),
//         timestamp: new Date().getTime(),
//         curp: allAnswersInfo.curp
//     })
//         .then(() => console.log("data was updated in the database"))
//         .catch(error => console.error("Error setting document: ", error))
//
// }

export const totalPoints = ({total}) => ({
    type: 'TOTAL_POINTS',
    total
});

export const loadQuestions = (allQuestions) => ({
    type: 'LOAD_QUESTIONS',
    allQuestions,
});


export const startLoadQuestions = () => {
    return (dispatch) => {
        db.collection('questionGroup/special/questions').get().then(data => {
            data.forEach(data =>  {
                dispatch(loadQuestions(data.data()));
            });
        });
        db.collection('questionGroup/numerical/questions').get().then(data => {
            data.forEach(data =>  {
                dispatch(loadQuestions(data.data()));
            });
        });
        db.collection('questionGroup/logical/questions').get().then(data => {
            data.forEach(data =>  {
                dispatch(loadQuestions(data.data()));
            });
        });
    }
};

export const addCompleted = () => ({
    type: 'ADD_COMPLETED',
});


export const addUserInfo = (userInfo = {}) => ({
    type: 'ADD_USER_INFO',
    userInfo
});

export const addCurp = (curp) => ({
    type: 'ADD_CURP',
    curp
});


export const clearLocalStorage = () => ({
    type: 'CLEAR_STORAGE',
});

export const clearOnStart = () => ({
    type: 'CLEAR_START',
});


// let questions = () => {
//     async function promise() {
//         let questionsArray = [];
//         db.collection('questionGroup/special/questions').get().then(data => {
//            return  data.forEach(data => questionsArray.push(data.data()))
//         }).then(() => {
//            return  db.collection('questionGroup/numerical/questions').get().then(data => {
//                 data.forEach(data => questionsArray.push(data.data()))
//             }).then(() => {
//                return db.collection('questionGroup/logical/questions').get().then(data => {
//                     data.forEach(data => questionsArray.push(data.data()))
//                 });
//             });
//         });
//         return await questionsArray;
//     }
//     return promise();
// }
//
// console.log(questions().then(data => console.log(data)));
//


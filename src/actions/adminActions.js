import db from "../firebase/firebase";

export const newQuestion = (question) => {
    return () => {
    const userRef = db.doc(`questionGroup/${question.questionGroup}/questions/${question.questionId}`);
    userRef.get()
        .then(doc => {
            if(doc.exists) {
                alert("that question id is already in questions database")
                userRef.update({
                    question
                })
                    .then(() => console.log("data was updated in the database"))
                    .catch(error => console.error("Error setting document: ", error))
            } else {
                userRef.set({
                    question
                })
                    .then(() => console.log("data was set to the database"))
                    .catch(error => console.error("Error adding document: ", error));
            }
        })
}
};


export const changeId = (id) => ({
    type: 'CHANGE_ID',
    id
});

export const questionId = (group) => {
    return (dispatch) => {
        console.log(group.questionGroup);
        const userRef = db.collection(`questionGroup/${group.questionGroup}/questions`);
        userRef.get().then(snap => {
            let size = snap.size;
            dispatch(changeId(size))
        })
    }
};
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
const rn = require('random-number');

admin.initializeApp({
    apiKey: "AIzaSyD2IMI9i9Gw6mskN9PlCNbmk7vDMzGpNYA",
    authDomain: "quizapp-d71a8.firebaseapp.com",
    credential: admin.credential.applicationDefault(),
    databaseURL: `https://quizapp-d71a8.firebaseio.com`
});

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
const options = {
    min: 0,
    max: 9,
    integer: true
};

exports.helloWorld = functions.https.onRequest((request, response) => {
    cors(request, response, () => {});
    let allData = [];
    let withCorrect = [];
    return admin.firestore().collection("questionGroup/special/questions").get().then((() => {
        var _ref = _asyncToGenerator(function* (snapshot) {
            snapshot.forEach((() => {
                var _ref2 = _asyncToGenerator(function* (data) {
                    let questionObject = data.data().question;
                    let selectedAnswer = "";
                    let text = questionObject.text;
                    let questionGroup = questionObject.questionGroup;
                    let questionId = questionObject.questionId;
                    let graph = questionObject.graph;
                    let points = questionObject.points;
                    let answerOption = questionObject.answerOptions;
                    let withoutCorrect = {
                        question: {
                            selectedAnswer,
                            text,
                            questionGroup,
                            questionId,
                            graph,
                            points,
                            answerOption
                        }
                    };
                    let random = rn(options);
                    allData.push(withoutCorrect);
                    withCorrect.push(questionObject);
                });

                return function (_x2) {
                    return _ref2.apply(this, arguments);
                };
            })());
            return admin.firestore().collection("questionGroup/logical/questions").get().then((() => {
                var _ref3 = _asyncToGenerator(function* (snapshot) {
                    snapshot.forEach((() => {
                        var _ref4 = _asyncToGenerator(function* (data) {
                            let questionObject = data.data().question;
                            let selectedAnswer = "";
                            let text = questionObject.text;
                            let questionGroup = questionObject.questionGroup;
                            let questionId = questionObject.questionId;
                            let graph = questionObject.graph;
                            let points = questionObject.points;
                            let answerOption = questionObject.answerOptions;
                            let withoutCorrect = {
                                question: {
                                    selectedAnswer,
                                    text,
                                    questionGroup,
                                    questionId,
                                    graph,
                                    points,
                                    answerOption
                                }
                            };
                            let random = rn(options);
                            allData.push(withoutCorrect);
                            withCorrect.push(questionObject);
                        });

                        return function (_x4) {
                            return _ref4.apply(this, arguments);
                        };
                    })());
                    return admin.firestore().collection("questionGroup/numerical/questions").get().then((() => {
                        var _ref5 = _asyncToGenerator(function* (snapshot) {
                            snapshot.forEach((() => {
                                var _ref6 = _asyncToGenerator(function* (data) {
                                    let questionObject = data.data().question;
                                    let selectedAnswer = "";
                                    let text = questionObject.text;
                                    let questionGroup = questionObject.questionGroup;
                                    let questionId = questionObject.questionId;
                                    let graph = questionObject.graph;
                                    let points = questionObject.points;
                                    let answerOption = questionObject.answerOptions;
                                    let withoutCorrect = {
                                        question: {
                                            selectedAnswer,
                                            text,
                                            questionGroup,
                                            questionId,
                                            graph,
                                            points,
                                            answerOption
                                        }
                                    };
                                    let random = rn(options);
                                    allData.push(withoutCorrect);
                                    withCorrect.push(questionObject);
                                });

                                return function (_x6) {
                                    return _ref6.apply(this, arguments);
                                };
                            })());
                            let awaited = yield allData;
                            let userQuestions = yield withCorrect;
                            let newArray = [];
                            let questionsDatabase = [];
                            let i;
                            for (i = 0; i < 20; i++) {
                                let random = rn(options);
                                newArray.push(awaited[rn(options)]);
                                questionsDatabase.push(userQuestions[rn(options)]);
                            }
                            // let array = await newArray;
                            // let object = {...array};
                            response.send((yield newArray));
                            admin.firestore().doc("users/test").set((yield newArray));
                        });

                        return function (_x5) {
                            return _ref5.apply(this, arguments);
                        };
                    })()).catch(function (error) {
                        console.log(error);
                        response.status(500).send(error);
                    });
                });

                return function (_x3) {
                    return _ref3.apply(this, arguments);
                };
            })());
        });

        return function (_x) {
            return _ref.apply(this, arguments);
        };
    })());
});

// async function specialgetQuestions() {
//     admin.firestore().collection("questionGroup/special/questions").get()
//         .then(async snapshot => {
//             let allData = [];
//             snapshot.forEach(async data => {
//                 let questionObject = data.data().question;
//                 let selectedAnswer = "";
//                 let text = questionObject.text;
//                 let questionGroup = questionObject.questionGroup;
//                 let questionId = questionObject.questionId;
//                 let graph = questionObject.graph;
//                 let points = questionObject.points;
//                 let answerOption = questionObject.answerOptions;
//                 let withoutCorrect = {
//                     question: {
//                         selectedAnswer,
//                         text,
//                         questionGroup,
//                         questionId,
//                         graph,
//                         points,
//                         answerOption
//                     }
//                 };
//                 let random = rn(options);
//                 allData.push(withoutCorrect);
//             });
//             return await allData;
//         }


// const functions = require('firebase-functions');
// const admin = require('firebase-admin');
// const cors = require('cors')({origin: true});
// const rn = require('random-number');
//
// admin.initializeApp();
//
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
// const options = {
//     min:  0
//     , max:  10
//     , integer: true
// };
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//     cors(request, response, () => {});
//     let allData = [];
//
//     admin.firestore().collection("questionGroup/special/questions").get()
//         .then(async snapshot => {
//             snapshot.forEach(async function(data) {
//                 let questionObject = data.data().question;
//                 let selectedAnswer = "";
//                 let text = questionObject.text;
//                 let questionGroup = questionObject.questionGroup;
//                 let questionId = questionObject.questionId;
//                 let graph = questionObject.graph;
//                 let points = questionObject.points;
//                 let answerOption = questionObject.answerOptions;
//                 let withoutCorrect = {question: {
//                         selectedAnswer,
//                         text,
//                         questionGroup,
//                         questionId,
//                         graph,
//                         points,
//                         answerOption
//                     }};
//                 allData.push(withoutCorrect);
//                 let awaited = await allData;
//                 response.send(awaited);
//             })
//         })
//         .catch(error => {
//             console.log(error);
//             response.status(500).send(error);
//         })
// });

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({origin: true});
const rn = require('random-number');

admin.initializeApp({
    apiKey: "AIzaSyD2IMI9i9Gw6mskN9PlCNbmk7vDMzGpNYA",
    authDomain: "quizapp-d71a8.firebaseapp.com",
    credential: admin.credential.applicationDefault(),
    databaseURL: `https://quizapp-d71a8.firebaseio.com`,
});

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
const options = {
    min:  0
    , max:  9
    , integer: true
};


exports.helloWorld = functions.https.onRequest((request, response) => {
    cors(request, response, () => {});
    let allData = [];
    let withCorrect = [];
   return admin.firestore().collection("questionGroup/special/questions").get()
        .then(async snapshot => {
            snapshot.forEach(async data => {
                let questionObject = data.data().question;
                let selectedAnswer = "";
                let text = questionObject.text;
                let questionGroup = questionObject.questionGroup;
                let questionId = questionObject.questionId;
                let graph = questionObject.graph;
                let points = questionObject.points;
                let answerOption = questionObject.answerOptions;
                let withoutCorrect = {
                    question: {
                        selectedAnswer,
                        text,
                        questionGroup,
                        questionId,
                        graph,
                        points,
                        answerOption
                    }
                };
                let random = rn(options);
                allData.push(withoutCorrect);
                withCorrect.push(questionObject);
            });
         return admin.firestore().collection("questionGroup/logical/questions").get()
                .then(async snapshot => {
                    snapshot.forEach(async data => {
                        let questionObject = data.data().question;
                        let selectedAnswer = "";
                        let text = questionObject.text;
                        let questionGroup = questionObject.questionGroup;
                        let questionId = questionObject.questionId;
                        let graph = questionObject.graph;
                        let points = questionObject.points;
                        let answerOption = questionObject.answerOptions;
                        let withoutCorrect = {
                            question: {
                                selectedAnswer,
                                text,
                                questionGroup,
                                questionId,
                                graph,
                                points,
                                answerOption
                            }
                        };
                        let random = rn(options);
                        allData.push(withoutCorrect);
                        withCorrect.push(questionObject);
                    });
                    return admin.firestore().collection("questionGroup/numerical/questions").get()
                        .then(async snapshot => {
                            snapshot.forEach(async data => {
                                let questionObject = data.data().question;
                                let selectedAnswer = "";
                                let text = questionObject.text;
                                let questionGroup = questionObject.questionGroup;
                                let questionId = questionObject.questionId;
                                let graph = questionObject.graph;
                                let points = questionObject.points;
                                let answerOption = questionObject.answerOptions;
                                let withoutCorrect = {
                                    question: {
                                        selectedAnswer,
                                        text,
                                        questionGroup,
                                        questionId,
                                        graph,
                                        points,
                                        answerOption
                                    }
                                };
                                let random = rn(options);
                                allData.push(withoutCorrect);
                                withCorrect.push(questionObject);
                            });
                            let awaited = await allData;
                            let userQuestions = await withCorrect;
                            let newArray = [];
                            let questionsDatabase = [];
                            let i;
                            for (i = 0; i < 20; i++) {
                                let random = rn(options);
                                newArray.push(awaited[rn(options)]);
                                questionsDatabase.push(userQuestions[rn(options)])
                            }
                            // let array = await newArray;
                            // let object = {...array};
                            response.send(await newArray);
                           admin.firestore().doc("users/test").set(await newArray);
                        })
                        .catch(error => {
                            console.log(error);
                            response.status(500).send(error);
                        })
                })
        })
});

// async function specialgetQuestions() {
//     admin.firestore().collection("questionGroup/special/questions").get()
//         .then(async snapshot => {
//             let allData = [];
//             snapshot.forEach(async data => {
//                 let questionObject = data.data().question;
//                 let selectedAnswer = "";
//                 let text = questionObject.text;
//                 let questionGroup = questionObject.questionGroup;
//                 let questionId = questionObject.questionId;
//                 let graph = questionObject.graph;
//                 let points = questionObject.points;
//                 let answerOption = questionObject.answerOptions;
//                 let withoutCorrect = {
//                     question: {
//                         selectedAnswer,
//                         text,
//                         questionGroup,
//                         questionId,
//                         graph,
//                         points,
//                         answerOption
//                     }
//                 };
//                 let random = rn(options);
//                 allData.push(withoutCorrect);
//             });
//             return await allData;
//         }
















// const functions = require('firebase-functions');
// const admin = require('firebase-admin');
// const cors = require('cors')({origin: true});
// const rn = require('random-number');
//
// admin.initializeApp();
//
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
// const options = {
//     min:  0
//     , max:  10
//     , integer: true
// };
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//     cors(request, response, () => {});
//     let allData = [];
//
//     admin.firestore().collection("questionGroup/special/questions").get()
//         .then(async snapshot => {
//             snapshot.forEach(async function(data) {
//                 let questionObject = data.data().question;
//                 let selectedAnswer = "";
//                 let text = questionObject.text;
//                 let questionGroup = questionObject.questionGroup;
//                 let questionId = questionObject.questionId;
//                 let graph = questionObject.graph;
//                 let points = questionObject.points;
//                 let answerOption = questionObject.answerOptions;
//                 let withoutCorrect = {question: {
//                         selectedAnswer,
//                         text,
//                         questionGroup,
//                         questionId,
//                         graph,
//                         points,
//                         answerOption
//                     }};
//                 allData.push(withoutCorrect);
//                 let awaited = await allData;
//                 response.send(awaited);
//             })
//         })
//         .catch(error => {
//             console.log(error);
//             response.status(500).send(error);
//         })
// });









